#ifndef _GAMEOBJECTFACTORY_HPP_
#define _GAMEOBJECTFACTORY_HPP_

#include "GameObject.hpp"

#include <map>
#include <string>

class GameObjectFactory
{
    public:

    //This is a pointer to a function which creates an object.
    typedef GameObject* (*tCreator)();

	typedef std::map<std::string, tCreator> tCreatorMap;

	bool Register(const char* pType, tCreator aCreator);

    GameObject* create(const char* pType);

    static GameObjectFactory & getInstance();

    private:
    /* Note: We've made the GameObjectFactory a singlton */
    GameObjectFactory();
	~GameObjectFactory();

    static GameObjectFactory* c_pInstance;

	tCreatorMap mCreators;
};

#endif

